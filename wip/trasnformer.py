import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import warnings
from sklearn.model_selection import train_test_split
import xgboost as xgb
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn import metrics
from sklearn.preprocessing import MinMaxScaler
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
import scipy.stats as stats
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LinearRegression
import re 

def get_approximation(regressor, data):
    X = get_X(data)

    return np.clip(regressor.predict(X) ** 2, 0, 8000)

def get_X(data):
    X = (data["Days with sales"]).values
    X = X.reshape(X.shape[0], 1)
    return X

def fit_regressor(data):
    X = get_X(data)
    y = data["Sales"] ** 0.5

    regressor = LinearRegression().fit(X, y)
    print("Regression score and coeff", regressor.score(X, y), regressor.coef_)

    return regressor

def averagePrice(x):
    if np.isnan(x['Average price']):
        return (x['Max price'] + x['Min price']) * 0.5
    else:
        return x['Average price']

def fill_section_map(section_map, data):
    def fill_map(section):
        if section in section_map.keys():
            return section

        similar = data.loc[data["ExtraSection"] == section]
        section_map[section] = similar
        return section

    data['ExtraSection'].apply(lambda x: fill_map(x))

def fill_brand_map(brand_map, data):
    def fill_map(x):
        if x in brand_map.keys():
            return x
        similar = data.loc[data["Brand"] == x]
        brand_map[x] = similar
        return x

    data['Brand'].apply(lambda x: fill_map(x))


def get_price_scale(section_map, row):
    if not row["ExtraSection"] in section_map:
        return  
    section = section_map[row["ExtraSection"]]

    min_price = section["Average price"].min()
    max_price = section["Average price"].max()
    current_price = row["Average price"]
    if np.abs(max_price - min_price) < 0.01:
        return 0.0

    return np.clip((current_price - min_price) / (max_price - min_price), 0, 1) - 0.5

def section_relative_price_tr(section_map, data):    
    data["Price scale"] = pd.to_numeric(data.apply(lambda x: get_price_scale(section_map, x), axis=1), errors='coerce').replace(np.nan, 0)

    return data

class Transformer:
    def __init__(self, scaler=None):
        if scaler is not None:
            self.scaler = scaler
        else:
            self.scaler = StandardScaler()

        self.enc = OneHotEncoder(handle_unknown='ignore', sparse=False)

        self.imputer = IterativeImputer(max_iter=10, random_state=0)  # SimpleImputer(missing_values=np.nan, strategy='mean')

        # regressor for rough predictions
        self.reg = None
        self.section_map = {}
        self.brand_map = {}

    def fit_encoder(self, X):
        o_cols = X.select_dtypes([object]).columns
        self.enc.fit(X[o_cols])

    def encode(self, X):
        # select object cols
        o_cols = X.select_dtypes([object]).columns

        # encode those and create df
        encoded = self.enc.transform(X[o_cols])  # one_hot_encode(X)
        df = pd.DataFrame(encoded)

        # drop object cols and append encoded
        result = X.drop(columns=o_cols)

        result = pd.concat([result.reset_index(drop=True), df.reset_index(drop=True)], axis=1)
        
        result.columns = result.columns.astype(str)

        return result

    def encode_label(self, X):
        object_candidates = list(X.dtypes[X.dtypes == "object"].index.values)

        encoder = LabelEncoder()
        for col in object_candidates:
            X[col] = encoder.fit_transform(X[col])

        return X

    def imputer_fit(self, X):
        self.imputer.fit(X)

    def imputer_transform(self, X):
        data = self.imputer.transform(X)
        return data

    def fit_scaler(self, X):
        num_candidates = list(X.dtypes[X.dtypes != "object"].index.values)
        self.scaler.fit(X[num_candidates])

    def scale(self, X):
        num_candidates = list(X.dtypes[X.dtypes != "object"].index.values)
        X[num_candidates] = self.scaler.transform(X[num_candidates])

        return X

    def split_sections(self, X):
        X["Section"] = X['full_category'].str.split('/').apply(lambda x: x[0])
        X["SubSection"] = X['full_category'].str.split('/').apply(lambda x: x[1])
        X["ExtraSection"] = X['full_category'].str.split('/').apply(lambda x:  x[len(x) - 1])
        return X

    def process_data(self, X, approximate=True):
        X = X.copy()

        if approximate:
            X["Approximate"] = get_approximation(self.reg, X)
        
        X["Discount"] = X["Max price"] - X["Min price"]
        X['Rating'] = X['Rating'].str.replace(',','.').astype(float) - 2.5
        
        X['Average price'] = X.apply(lambda x : averagePrice(x), axis=1).astype(float)

        X = self.split_sections(X)

        X = section_relative_price_tr(self.section_map, X)

        X.fillna('Nan', inplace=True)

        return X

    def fit(self, X, fit_approximate=True):
        # avoid mutations of original data
        X = X.copy()

        # here it gets confusing
        # fit regressor with sales
        if fit_approximate == True:
            self.reg = fit_regressor(X)
        
        X = self.split_sections(X)
        fill_section_map(self.section_map, X)
        fill_brand_map(self.brand_map, X)


    def transform(self, X, approximate=True):
        if "Id" in X:
            X = X.drop(columns=["Id"])
        else:
            X = X.copy()

        X = self.process_data(X, approximate)

        X = section_relative_price_tr(self.section_map, X)

        if "Sales" in X:
            X =  X.drop(columns=["Sales"])

        return X.drop(columns=["Color", "Category", "full_category"])
    
    def transform_lgbm(self, X, drop_sales=True):
        if "Id" in X:
            X = X.drop(columns=["Id"])
        else:
            X = X.copy()

        def fixZeroRating(x):
            if x["Rating"] == "0,0000":
                return "2,5"
            return x["Rating"]

        X['Rating'] = X.apply(lambda x : fixZeroRating(x), axis=1)
        X['Rating'] = X['Rating'].str.replace(',', '.').astype(float) - 2.5
        
        X['Average price'] = X.apply(lambda x : averagePrice(x), axis=1).astype(float)
        
        X["Comments"] = X["Comments"] ** (1/2)
        #X["Days with sales"] = X["Days with sales"] ** (1/2)

        def convertIp(x):
            seller = x["Seller"]
            if seller is None:
                return seller
            if not bool(re.search('ИП', str(seller))):
                return seller
            return "ИП"

        X['Seller'] = X.apply(lambda x : convertIp(x), axis=1)


        X["Discount"] = X["Max price"] - X["Min price"]

        #X["Weeks with sales"] = (X["Days with sales"] / 7.0).astype(int)
        #X["Weeks in stock"] = (X["Days in stock"] / 7.0).astype(int)

        X = self.split_sections(X)
        X = section_relative_price_tr(self.section_map, X)

        if "Sales" in X and drop_sales:
            X = X.drop(columns=["Sales"])

        X.fillna('Nan', inplace=True)

        # return X.drop(columns=["Color", "Category", "Final price", "Basic Sale", "Basic Sale Price", "Base price"])

        return X.drop(columns=["Color", "Category", "Section", "SubSection", "Name", "full_category", "Final price", "Basic Sale Price", "Base price", "Max price", "Min price"])

    def transform_cats(self, X, drop_sales=True):
        if "Id" in X:
            X = X.drop(columns=["Id"])
        else:
            X = X.copy()

        def fixZeroRating(x):
            if x["Rating"] == "0,0000":
                return "2,5"
            return x["Rating"]

        X['Rating'] = X.apply(lambda x : fixZeroRating(x), axis=1)
        X['Rating'] = X['Rating'].str.replace(',', '.').astype(float) - 2.5
        
        X['Average price'] = X.apply(lambda x : averagePrice(x), axis=1).astype(float)
        
        X["Comments"] = X["Comments"] ** (1/2)
        X["Days with sales"] = X["Days with sales"] ** (1/2)

        def convertIp(x):
            seller = x["Seller"]
            if seller is None:
                return seller
            if not bool(re.search('ИП', str(seller))):
                return seller
            return "ИП"

        X['Seller'] = X.apply(lambda x : convertIp(x), axis=1)


        X["Discount"] = X["Max price"] - X["Min price"]

        #X["Weeks with sales"] = (X["Days with sales"] / 7.0).astype(int)
        #X["Weeks in stock"] = (X["Days in stock"] / 7.0).astype(int)

        X = self.split_sections(X)
        X = section_relative_price_tr(self.section_map, X)

        if "Sales" in X and drop_sales:
            X = X.drop(columns=["Sales"])

        X.fillna('Nan', inplace=True)

        # return X.drop(columns=["Color", "Category", "Final price", "Basic Sale", "Basic Sale Price", "Base price"])

        return X.drop(columns=["Color", "Category", "Section", "SubSection", "Name", "full_category", "Final price", "Basic Sale Price", "Base price", "Max price", "Min price"])