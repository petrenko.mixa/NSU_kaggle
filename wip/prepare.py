import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import warnings
from sklearn.model_selection import train_test_split
import xgboost as xgb
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn import metrics
from sklearn.preprocessing import MinMaxScaler
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
import scipy.stats as stats
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LinearRegression
from metrics import *

def one_hot_encode(df):
    categorical = df.select_dtypes(include='object')

    processed = pd.get_dummies(categorical)

    encoded = df.drop(categorical.columns.values, axis = 1)
    encoded = pd.concat([encoded, processed], axis=1)

    return encoded

def averagePrice(x):
    if np.isnan(x['Average price']):
        return (x['Max price'] + x['Min price']) * 0.5
    else:
        return x['Average price']

def remove_outliers(data):
    ids = data[(data['Average price'] > 150000) | (data["Sales"] > 8000)].index
    #ids = data[(data['Comments'] > 4000) | (data['Average price'] > 150000)].index
    print(ids)
    return data.drop(ids)

def remove_outliers_split_2(X, y):
    ids = X[(X['Comments'] > 4000) | (X['Average price'] > 150000)].index
    print(ids)
    return X.drop(ids), y.drop(ids)

def remove_outliers_split(X, y):
    ids = X[(X['Comments'] > 50) | (X['Average price'] > 150000)].index
    print(ids)
    return X.drop(ids), y.drop(ids)



def section_relative_price(data):    
    section_map = {}

    def fill_map(x):
        if x in section_map.keys():
            return x
        similar = data.loc[data["ExtraSection"] == x]
        section_map[x] = similar
        return x

    data['ExtraSection'].apply(lambda x: fill_map(x))

    def get_price_scale(x):
        if not x["ExtraSection"] in section_map:
            return 0

        section = section_map[x["ExtraSection"]]
        
        min_price = section["Average price"].min()
        max_price = section["Average price"].max()
        current_price = x["Average price"]

        if np.abs(max_price - min_price) < 0.01:
            return 0.0

        return np.clip((current_price - min_price) / (max_price - min_price), 0, 1) - 0.5


    data["Price scale"] = pd.to_numeric(data.apply(lambda x: get_price_scale(x), axis=1), errors='coerce')

    return data

def drop_colums(data):
    if "Sales" in data:
        data = data.drop(columns=["Sales"])

    return data.drop(columns=["Max price", "Min price", "Final price", "Color", "Category", "Base price", "Basic Sale Price", "Name", "full_category"])

def encode(enc, X):
    X = X.copy()
    o_cols = X.select_dtypes([object]).columns

    encoded = enc.transform(X[o_cols])
    df = pd.DataFrame(encoded)

    X = X.drop(columns=o_cols)

    X = pd.concat([X.reset_index(drop=True), df.reset_index(drop=True)], axis=1)
    X.columns = X.columns.astype(str)
    return X

def prepare_data(data):
    data["Comments"] = data["Comments"] ** (1/2)
    data["Days with sales"] = data["Days with sales"] ** 0.5

    data["Discount"] = data["Max price"] - data["Min price"]

    # consider rating of 3 as zero (very average stuff) and below as negative
    data['Rating'] = data['Rating'].str.replace(',', '.').astype(float) - 3

    # fill average price
    data['Average price'] = data.apply(lambda x : averagePrice(x), axis=1).astype(float)

    # fill sections
    section = data['full_category'].str.split('/').apply(lambda x: x[0])
    sub_section = data['full_category'].str.split('/').apply(lambda x: x[1])
    extra_section = data['full_category'].str.split('/').apply(lambda x:  x[len(x) - 1])

    data["Section"] = section
    data["SubSection"] = sub_section
    data["ExtraSection"] = extra_section

    data = section_relative_price(data)

    data["Average price"] = np.mean([data["Base price"], data["Basic Sale Price"], data["Average price"]], axis=0)

    data.fillna('Nan', inplace=True)

    return drop_colums(data)


def invert(arr):
    return np.expm1(arr)


def get_X(data):
    X = (data["Days with sales"] ** 0.3).values
    X = X.reshape(X.shape[0], 1)
    return X


def fit_regressor(data):
    X = get_X(data)
    y = np.log1p(data["Sales"])

    regressor = LinearRegression().fit(X, y)
    print(regressor.score(X, y), regressor.coef_)

    return regressor


def get_approximation(regressor, data):
    X = get_X(data)

    return np.clip(invert(regressor.predict(X)), 0, 8000)


class DataTransformer:
    def __init__(self, scaler=None):
        if scaler is not None:
            self.scaler = scaler
        else:
            self.scaler = StandardScaler()

        self.enc = OneHotEncoder(handle_unknown='ignore', sparse=False)

        self.imputer = IterativeImputer(max_iter=10,
                                        random_state=0)  # SimpleImputer(missing_values=np.nan, strategy='mean')

        self.reg = None
        self.section_map = {}

    def fit_encoder(self, X):
        o_cols = X.select_dtypes([object]).columns
        self.enc.fit(X[o_cols])

    def encode(self, X):
        # select object cols
        o_cols = X.select_dtypes([object]).columns

        # encode those and create df
        encoded = self.enc.transform(X[o_cols])  # one_hot_encode(X)
        df = pd.DataFrame(encoded)

        # drop object cols and append encoded
        result = X.drop(columns=o_cols)

        print(result.shape, df.shape)

        result = pd.concat([result.reset_index(drop=True), df.reset_index(drop=True)], axis=1)

        return result

    def encode_label(self, X):
        object_candidates = list(X.dtypes[X.dtypes == "object"].index.values)

        encoder = LabelEncoder()
        for col in object_candidates:
            X[col] = encoder.fit_transform(X[col])

        return X

    def imputer_fit(self, X):
        self.imputer.fit(X)

    def imputer_transform(self, X):
        data = self.imputer.transform(X)
        return data

    def scaler_fit(self, X):
        self.scaler.fit(X)

    def scaler_transform(self, X):
        data = self.scaler.transform(X)

        return data

    def process_data(self, X):
        X["Approximate"] = get_approximation(self.reg, X)
        X = prepare_data(X)

        X = section_relative_price(X)
        return X

    def fit(self, X):
        # avoid mutations of original data
        X = X.copy()

        # here it gets confusing
        # fit regressor with sales
        self.reg = fit_regressor(X)

        X = self.process_data(X)

        num_candidates = list(X.dtypes[X.dtypes != "object"].index.values)

        numeric = X[num_candidates]

        self.scaler_fit(numeric)

        self.fit_encoder(X)



    def transform(self, X, encode=True, scale=False):
        X = self.process_data(X)

        num_candidates = list(X.dtypes[X.dtypes != "object"].index.values)

        if scale:
            X[num_candidates] = self.scaler_transform(X[num_candidates])
        if encode:
            X = self.encode(X)

        return X

def evaluate(model, X, y, custom_invert = None):
    preds = model.predict(X)
    invert_fn = invert if custom_invert == None else custom_invert
    res = np.clip(invert_fn(preds), 0 , 8000)
   
    print("SMAPE: " + str(smape(res, invert_fn(y))))

def save_res(submission, custom_invert = None):
    validation = pd.read_csv("../data/test.csv")
    val_ids = validation["Id"]

    if custom_invert == None:
        inverted = invert(submission)
    else:
        inverted = custom_invert(submission)
        
    converted = np.clip(inverted, 0 , 8000)

    d = {'Id': val_ids.to_numpy(), 'Expected': converted}
    df = pd.DataFrame(data=d)
    df.to_csv('../submissions/submission.csv', index=False)
